<?php

class Conexion{

	public static function Conectar(){
		/*
		$servidor="132.248.122.58:16612";
		$sistema="eda_impar";
		$sis_usr="desarrollo";
		$sis_key="d3s4rR0Ll0";
		*/
		

		$servidor="localhost";
		$sistema="medico";
		$sis_usr="root";
		$sis_key="toor";
		
		
		$now=strftime("%Y%m%d%H%M%S", time());
		$allowance=999999;


		$database   =$sistema;
		$usuario_db =$sis_usr;
		$pass_db    =$sis_key;

		date_default_timezone_set("America/Mexico_City");
		$link = new mysqli($servidor,$usuario_db,$pass_db,$database);
		if(!$link){ echo "En este momento la demanda es demasiado alta. Intenta tu acceso mas tarde."; die(); }else{ return $link; }
		$appTitle="..:: EDA - DGCCH ::..";
		$now  =strftime("%Y%m%d%H%M%S", time());
	}
}

class Conector{
	private $db;
    
	

	public function __construct(){
		$this->db=Conexion::Conectar();
	}


	function insertPacientes($nompaciente, $edadpaciente, $telpaciente, $dirpaciente){
		$this->nompaciente="'".$nompaciente."'";
		$this->edadpaciente="'".$edadpaciente."'";
		$this->telpaciente="'".$telpaciente."'";
		$this->dirpaciente="'".$dirpaciente."'";

/*
		echo "<br>el nombre es". $this->nompaciente;
		echo "<br>la edad es". $this->edadpaciente;
		echo "<br>telefono es". $this->telpaciente;
		echo "<br>la direccion es". $this->dirpaciente;
		*/
		$insertPaciente="INSERT INTO pacientes(nompaciente, edadpaciente, telpaciente, dirpaciente) 
					VALUES ($this->nompaciente, $this->edadpaciente, $this->telpaciente, $this->dirpaciente)";
		$ejec_insertPaciente=$this->db->query($insertPaciente);
		//if($ejec_insertPaciente){echo "inserted";}else{echo "error ".$link->error;}
		//echo "el resultado de inserción es:".$ejec_insertPaciente;

	}

	function retuTodosPacientes(){
		$pacientes=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_allpacientes="SELECT * FROM pacientes ORDER BY idpaciente ASC;";
		$ejec_return_allpacientes=$this->db->query($return_allpacientes); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_allpacientes->fetch_assoc()){
				$pacientes[]=$reg;
		}
		return $pacientes;
	}


	function retuIdPaciente($idpaciente){
		$this->idpaciente="'".$idpaciente."'";
		$paciente=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_idpaciente="SELECT * FROM pacientes WHERE idpaciente=$this->idpaciente;";
		$ejec_return_idpaciente=$this->db->query($return_idpaciente); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_idpaciente->fetch_assoc()){
				$paciente[]=$reg;
		}
		return $paciente;
	}

	function editarPaciente($idpaciente,$nompaciente, $edadpaciente, $telpaciente, $dirpaciente){
		$this->idpaciente="'".$idpaciente."'";
		$this->nompaciente="'".$nompaciente."'";
		$this->edadpaciente="'".$edadpaciente."'";
		$this->telpaciente="'".$telpaciente."'";
		$this->dirpaciente="'".$dirpaciente."'";

		$modificarPaciente="UPDATE pacientes SET nompaciente=$this->nompaciente, edadpaciente=$this->edadpaciente,telpaciente=$this->telpaciente, dirpaciente=$this->dirpaciente WHERE idpaciente=$this->idpaciente;";
		$ejec_modificarPaciente=$this->db->query($modificarPaciente);
		//if($ejec_insertPaciente){echo "inserted";}else{echo "error ".$link->error;}
		//echo "el resultado de inserción es:".$ejec_insertPaciente;

	}


	function eliminarPaciente($idpaciente){
		$this->idpaciente="'".$idpaciente."'";
		//$tamanio=sizeof($this->datPI);
		$eliminarPaciente="DELETE FROM pacientes WHERE idpaciente = $this->idpaciente;";
		$ejec_eliminarPaciente=$this->db->query($eliminarPaciente);
	}


	function insertRegistroClinico($pesohistorial, $tallahistorial, $ahhistorial, $pahistorial, $idpaciente){
		$this->pesohistorial="'".$pesohistorial."'";
		$this->tallahistorial="'".$tallahistorial."'";
		$this->ahhistorial="'".$ahhistorial."'";
		$this->pahistorial="'".$pahistorial."'";
		$this->idpaciente="'".$idpaciente."'";

		echo "<br>el peso es". $this->pesohistorial;
		echo "<br>la talla es". $this->tallahistorial;
		echo "<br>ahhistorial es". $this->ahhistorial;
		echo "<br>la pahistorial es". $this->pahistorial;
		echo "<br>el idpaciente ". $this->idpaciente;
		echo "<br>";
		$insertPaciente="INSERT INTO historial(pesohistorial, tallahistorial, ahhistorial, pahistorial, idpaciente) 
		VALUES ($this->pesohistorial, $this->tallahistorial, $this->ahhistorial, $this->pahistorial, $this->idpaciente)";
		$ejec_insertPaciente=$this->db->query($insertPaciente);
		//if($ejec_insertPaciente){echo "inserted";}else{echo "error ".$link->error;}
		//echo "el resultado de inserción es:".$ejec_insertPaciente;
	}


	function obtenerHistoriales(){
		$historiales=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_historiales="SELECT DISTINCT * FROM historial h INNER JOIN pacientes p ON h.idpaciente=p.idpaciente ORDER BY h.idpaciente DESC;";
		$ejec_return_historiales=$this->db->query($return_historiales); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_historiales->fetch_assoc()){
				$historiales[]=$reg;
		}
		return $historiales;
	}


	function retuIdExpediente($idpaciente){
		$this->idpaciente="'".$idpaciente."'";
		$expedientes=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_idexpediente="SELECT * FROM historial INNER JOIN pacientes ON historial.idpaciente = pacientes.idpaciente WHERE historial.idpaciente = $this->idpaciente ORDER BY idhistorial DESC;";
		$ejec_return_idexpediente=$this->db->query($return_idexpediente); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_idexpediente->fetch_assoc()){
				$expedientes[]=$reg;
		}
		return $expedientes;
	}


	function editarExpediente($idhistorial,$pesohistorial, $tallahistorial, $ahhistorial, $pahistorial){
		$this->idhistorial="'".$idhistorial."'";
		$this->pesohistorial="'".$pesohistorial."'";
		$this->tallahistorial="'".$tallahistorial."'";
		$this->ahhistorial="'".$ahhistorial."'";
		$this->pahistorial="'".$pahistorial."'";

		$modificarExpediente="UPDATE historial SET pesohistorial=$this->pesohistorial, tallahistorial=$this->tallahistorial,ahhistorial=$this->ahhistorial, pahistorial=$this->pahistorial WHERE idhistorial=$this->idhistorial;";
		$ejec_modificarExpediente=$this->db->query($modificarExpediente);
		//if($ejec_insertPaciente){echo "inserted";}else{echo "error ".$link->error;}
		//echo "el resultado de inserción es:".$ejec_insertPaciente;

	}

	function retuExpediente($idhistorial){
		$this->idhistorial="'".$idhistorial."'";
		$expediente=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_expediente="SELECT * FROM historial WHERE idhistorial = $this->idhistorial;";
		$ejec_return_expediente=$this->db->query($return_expediente); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_expediente->fetch_assoc()){
				$expediente[]=$reg;
		}
		return $expediente;
	}


	
	function imprimirRecetaPDF($idhistorial){
		//$this->idhistorial="'".$idhistorial."'";
		$contenidoPDF=array();
		/*SE DEBE MODIFICAR gruposextraordinarios5 POR EL AREA QUE APLIQUE EN ESE DIA */
		$return_imprimir="SELECT * FROM historial INNER JOIN pacientes ON historial.idpaciente = pacientes.idpaciente WHERE historial.idhistorial = $this->idhistorial;";
		$ejec_return_imprimir=$this->db->query($return_imprimir); //or die(mysqli_error());//die imprime los errores que le pedimos
			while($reg=$ejec_return_imprimir->fetch_assoc()){
				$contenidoPDF[]=$reg;
		}
		return $contenidoPDF;
	}

}