<?php
header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

require("conexion.php"); // importa el archivo de la conexion a la BD
$con = new Conexion();
$conector = new Conector();


$resultadoAllPacientes=$conector->retuTodosPacientes();


class Result {}


$json = json_encode($resultadoAllPacientes); // Muestra el json generado

//Envio de informacion del JSON
header('Content-Type: application/json; charset=utf-8');
echo $json;
?>